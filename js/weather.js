const website = "https://api.openweathermap.org/data/2.5/weather?q=";
const appid = "aaa010f2287a11ac9912d466dac429db";

var catfact = $.get("https://cat-fact.herokuapp.com/facts/random", function(data){
    return data;
});

$("#catfact").html('<h4 id="catfact">Random Cat Fact... ' + catfact.text + '</h4>');

$("#submit").click(function(){
    $("#Result").html("");
    var city = $("#city").val();
    $("#Result").append(GetData(city));
});

function GetData(city){
    var Url = website + city + "&appid=" + appid;
    return $.get(Url, function(data){
        return data;
    });
};